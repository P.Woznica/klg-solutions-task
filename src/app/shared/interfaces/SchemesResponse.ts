export interface SchemesResponse {
  "name": string,
  "description": string,
  "trigger": number,
  "interimtrigger": number,
  "lbmanEffectivedeadlineinfo": number,
}

import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {SchemesComponent} from "./schemes.component";
import {AddSchemeComponent} from "./add-scheme/add-scheme.component";

const routes: Routes = [
  {
    path: '',
    component: SchemesComponent
  },
  {
    path: 'add',
    component: AddSchemeComponent
  },
  ]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SchemesRoutingModule { }

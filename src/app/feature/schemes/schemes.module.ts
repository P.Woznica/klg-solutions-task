import { NgModule } from '@angular/core';
import {SchemesComponent} from "./schemes.component";
import {SchemesRepositoryService} from "./schemes-repository.service";
import {AddSchemeComponent} from "./add-scheme/add-scheme.component";
import {SharedModule} from "../../shared/modules/shared.module";
import {SchemesRoutingModule} from "./schemes-routing.module";

@NgModule({
  declarations: [
    SchemesComponent,
    AddSchemeComponent
  ],
  imports: [
    SchemesRoutingModule,
    SharedModule,
  ],
  providers: [
    SchemesRepositoryService
  ]
})
export class SchemesModule { }

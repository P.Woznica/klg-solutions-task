import {Component, OnDestroy, OnInit} from '@angular/core';
import {SchemesRepositoryService} from "./schemes-repository.service";
import {Subscription} from "rxjs";
import {SchemesResponse} from "../../shared/interfaces/SchemesResponse";
import {AddSchemeDropdownItemsEnum} from "./add-scheme/AddSchemeDropdownItemsEnum";


@Component({
  selector: 'app-schemes',
  templateUrl: './schemes.component.html',
  styleUrls: ['./schemes.component.scss']
})
export class SchemesComponent implements OnInit, OnDestroy {
  public plannedPersonalData = AddSchemeDropdownItemsEnum.PLANNED_PERSONAL_DATA
  public schemesList: SchemesResponse[] = [];
  public displayedColumns: string[] =
    ['name', 'description', 'triggerName', 'interimTriggerName', 'effectiveDeadlineInfo', 'tableButtons'];
  private subscription!: Subscription

  constructor(
    private schemesRepositoryService: SchemesRepositoryService
  ) {
  }

  ngOnInit(): void {
    this.getSchemesList()
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

  public determineTriggerName(triggerName: number): string{
    if(triggerName === 5){
      return AddSchemeDropdownItemsEnum.CHANGE_OR_NEW
    }
    else if (triggerName === null){
      return ''
    }
    else {
      return 'Data Subject Access Request - request submitted'
    }
  }

  public getSchemesList(): void{
    this.subscription = this.schemesRepositoryService.getSchemes()
      .subscribe({
        next: schemesList => {this.schemesList = schemesList},
        error: err => console.error(err)
        }
      )
  }
}

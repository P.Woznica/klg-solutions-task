export enum AddSchemeDropdownItemsEnum {
  PLANNED_PERSONAL_DATA = 'Planned Personal Data transfer to other processing entity',
  PERSONAL_DATA_TRANSFER = 'Personal Data transfer to other processing entity',
  DATA_COLLECTION = 'Data Collection/Acquisition Instant',
  PLANNED_CHANGE = 'Planned Change or new personal data processing purpose',
  CHANGE_OR_NEW = 'Change(d) or new personal data processing purpose',
  PURPOSE_SERVICE = 'Purpose/Service start/commencement date -legal/contract start'
}

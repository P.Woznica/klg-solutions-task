import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AddSchemeFormEnum} from "./AddSchemeFormEnum";
import {AddSchemeDropdownItemsEnum} from "./AddSchemeDropdownItemsEnum";
import {map, Observable, startWith} from "rxjs";

@Component({
  selector: 'app-add-scheme',
  templateUrl: './add-scheme.component.html',
  styleUrls: ['./add-scheme.component.scss']
})
export class AddSchemeComponent implements OnInit {
  public formGroup!: FormGroup;
  public filteredItems!: Observable<string[]>;
  public name = AddSchemeFormEnum.NAME
  public triggerName = AddSchemeFormEnum.TRIGGER_NAME
  public triggerDateKnown = AddSchemeFormEnum.TRIGGER_DATE_KNOWN
  public isInterimTrigger = AddSchemeFormEnum.IS_INTERIM_TRIGGER
  public interimTriggerName = AddSchemeFormEnum.INTERIM_TRIGGER_NAME
  public constraintName = AddSchemeFormEnum.CONSTRAINT_NAME
  public constraintValueKnown = AddSchemeFormEnum.CONSTRAINT_VALUE_KNOWN
  public effectiveDeadlineInfo = AddSchemeFormEnum.EFFECTIVE_DEADLINE_INFO
  public relatedProcessingBasisReferenceType = AddSchemeFormEnum.RELATED_PROCESSING_BASIS_REFERENCE_TYPE
  public purma = AddSchemeFormEnum.PURMA
  public nntm = AddSchemeFormEnum.NNTM
  public pdb = AddSchemeFormEnum.PDB
  public dsart = AddSchemeFormEnum.DSART
  public description = AddSchemeFormEnum.DESCRIPTION
  public dropdownItems: string[] = [
    AddSchemeDropdownItemsEnum.PLANNED_PERSONAL_DATA,
    AddSchemeDropdownItemsEnum.PERSONAL_DATA_TRANSFER,
    AddSchemeDropdownItemsEnum.DATA_COLLECTION,
    AddSchemeDropdownItemsEnum.PLANNED_CHANGE,
    AddSchemeDropdownItemsEnum.CHANGE_OR_NEW,
    AddSchemeDropdownItemsEnum.PURPOSE_SERVICE,
  ]

  constructor(
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.formGroup = this.buildForm();
    this.autoCompleteOptions();

  }

  public onSubmit(): void{
    return console.log(this.formGroup.value)
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      [this.name]: [null, Validators.required],
      [this.triggerName]: [null, Validators.required],
      [this.triggerDateKnown]: [null],
      [this.isInterimTrigger]: [false],
      [this.interimTriggerName]: [null],
      [this.constraintName]: [null, Validators.required],
      [this.constraintValueKnown]: [null],
      [this.effectiveDeadlineInfo]: [null, Validators.required],
      [this.relatedProcessingBasisReferenceType]: [null],
      [this.purma]: [false],
      [this.nntm]: [false],
      [this.pdb]: [false],
      [this.dsart]: [false],
      [this.description]: [null]
    });
  }

  private autoCompleteOptions(): void{
    this.filteredItems = this.formGroup.controls[this.relatedProcessingBasisReferenceType].valueChanges
      .pipe(
      startWith(''),
      map(value => this.autoCompleteFilter(value))
    )
  }

  private autoCompleteFilter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.dropdownItems.filter(item => item.toLowerCase().includes(filterValue));
  }
}

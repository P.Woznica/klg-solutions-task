import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {SchemesResponse} from "../../shared/interfaces/SchemesResponse";

@Injectable()
export class SchemesRepositoryService {
  private readonly schemesEndpoint = 'assets/response_1548851123961.json';

  constructor(
    private httpClient: HttpClient
  ) { }

  public getSchemes(): Observable<SchemesResponse[]>{
    return this.httpClient.get<SchemesResponse[]>(this.schemesEndpoint)
  }
}
